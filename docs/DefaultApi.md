# ProcessId.DefaultApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delProcessIdProcessIdDelete**](DefaultApi.md#delProcessIdProcessIdDelete) | **DELETE** /process_id | Del Process Id
[**getProcessIdProcessIdGet**](DefaultApi.md#getProcessIdProcessIdGet) | **GET** /process_id | Get Process Id
[**setProcessIdProcessIdPost**](DefaultApi.md#setProcessIdProcessIdPost) | **POST** /process_id | Set Process Id

<a name="delProcessIdProcessIdDelete"></a>
# **delProcessIdProcessIdDelete**
> Object delProcessIdProcessIdDelete()

Del Process Id

### Example
```javascript
import {ProcessId} from 'process_id';

let apiInstance = new ProcessId.DefaultApi();
apiInstance.delProcessIdProcessIdDelete((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getProcessIdProcessIdGet"></a>
# **getProcessIdProcessIdGet**
> Object getProcessIdProcessIdGet()

Get Process Id

### Example
```javascript
import {ProcessId} from 'process_id';

let apiInstance = new ProcessId.DefaultApi();
apiInstance.getProcessIdProcessIdGet((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="setProcessIdProcessIdPost"></a>
# **setProcessIdProcessIdPost**
> Pid setProcessIdProcessIdPost(body)

Set Process Id

### Example
```javascript
import {ProcessId} from 'process_id';

let apiInstance = new ProcessId.DefaultApi();
let body = new ProcessId.Pid(); // Pid | 

apiInstance.setProcessIdProcessIdPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Pid**](Pid.md)|  | 

### Return type

[**Pid**](Pid.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

